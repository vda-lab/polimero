# polimero - composable data visualization using web components

![logo](https://bitbucket.org/vda-lab/polimero/raw/master/polimero.png "Alt text")

The polimero toolkit combines D3 for visualization with Polymer for webcomponents. Result: easy composable linked data visualizations.

```html
<html>
<head>
  <link rel="import" href="bower_components/polimero/polimero.html">
</head>
<body>
  <template is="dom-bind">
    <polimero-ajax
      url="http://demo.thedatatank.com/xls/baseball.json"
      datapoints="{{csv}}"></polimero-ajax>
    <polimero-scatterplot
      datapoints="{{csv}}"
      dimensions="Position,Salary"></polimero-scatterplot>
  </template>
</body>
</html>
```

The toolkit is made available under the MIT license (see LICENSE).

## Using the toolkit

To use polimero in a new app, use these steps:

* `mkdir myNewApp`
* `cd myNewApp`
* `bower install --save http://bitbucket.org/vda-lab/polimero.git`
* Create an index.html such as the one above.
* `http-server`
* Go to http://localhost:8080

You can find a list of the available elements in the `elements/` directory, including polimero-barchart, polimero-scatterplot, polimero-splom, polimero-parcoords and others.

Here's some example code loading different types of files:

```html
<html>
<head>
  <link rel="import" href="bower_components/polimero/polimero.html">
</head>
<body>
  <template is="dom-bind">
    <!-- Loading data from a remote source -->
    <polimero-ajax
      url="http://demo.thedatatank.com/xls/baseball.json"
      datapoints="{{ajax}}"></polimero-ajax>
    <polimero-scatterplot
      datapoints="{{ajax}}"
      dimensions="Position,Salary"></polimero-scatterplot>

    <!-- Loading a SAM file -->
    <polimero-bio-sam
      file="data/NA12878_chr20.sam_part"
      subsample="5000"
      start="28251000" stop="28257000"
      datapoints="{{sam}}"></polimero-bio-sam>
    <polimero-scatterplot
      title="SAM flags" dimensions="pos,flag"
      datapoints="{{sam}}"></polimero-scatterplot>
    <polimero-klaudia
      title="Mapped readpairs, coloured by flag"
      datapoints="{{sam}}"></polimero-klaudia>

    <!-- Loading a VCF file -->
    <polimero-bio-vcf
      file="data/HG00096.raw.vcf"
      datapoints="{{vcf}}"></polimero-bio-vcf>
    <polimero-scatterplot
      datapoints="{{vcf}}"
      dimensions="pos,info_MQ"></polimero-scatterplot>
    <polimero-scatterplot
      datapoints="{{vcf}}"
      dimensions="pos,info_MQ0"></polimero-scatterplot>

    <!-- Loading a CSV file -->
    <polimero-csv
      file="data/data.csv"
      datapoints="{{csv}}"></polimero-csv>
    <polimero-scatterplot
      datapoints="{{csv}}"
      dimensions="sepalLength,sepalWidth"
      brushedmarks="{{bm}}"></polimero-scatterplot>
    <polimero-radviz
      datapoints="{{csv}}"
      dimensions="sepalLength,sepalWidth,petalLength,petalWidth"
      brushedmarks="{{bm}}"></polimero-radviz>

    <!-- Loading a graph -->
    <polimero-graph
      file="data/graph.csv"
      datapoints="{{graph}}"></polimero-graph>
    <polimero-circos
      datapoints="{{graph}}"
      brushedmarks="{{bm}}"></polimero-circos>
  </template>
</body>
</html>
```

The output looks like this:

![screenshot](https://bitbucket.org/vda-lab/polimero/raw/master/screenshot.png "Alt text")

## For developers

Please see README_DEV.md file.

## TODO

See "Issues" on http://bitbucket.org/vda-lab/polimero/
