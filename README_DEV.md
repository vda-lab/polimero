# Developer's view on polimero

In this document we'll attempt to explain the way the code is built up so that (1) you can create your own elements, and (2) we still know what we did in a month's time...

## Example visualization using polimero

A simple HTML file looks like this:

```html
<html>
<head>
  <link rel="import" href="polimero.html">
</head>
<body>
  <template is="dom-bind">
    <polimero-csv file="data/data.csv" datapoints="{{csv}}"></polimero-csv>
    <polimero-scatterplot
			datapoints="{{csv}}"
			dimensions="sepalLength,sepalWidth"</polimero-scatterplot>
    <polimero-radviz datapoints="{{csv}}"
			dimensions="sepalLength,sepalWidth,petalLength,petalWidth"
			brushedmarks="{{bm}}"></polimero-radviz>
  </template>
</body>
</html>

```

What happens here?

* We import the polimero library in the `head`.
* We create a main `<template>` element in the `body`, adding the argument `is="dom-bind"`.
* We load the data from a CSV file into a variable `"{{csv}}"`.
* We then visualize that data using a scatterplot and radviz.


## Modular architecture

The aim of polimero is to provide a modular architecture for visual data analysis.

### Types of modules

There are basically three types of modules:

1. *data modules* - to load data
1. *visual modules* - to show data
1. *computation modules* - to perform calculations on the data

#### Data modules

Examples of data modules are `polimero-csv`, `polimero-graph` and `polimero-bio-sam`. These basically load the data from a datafile and store it in the `datapoints` variable. This variable is then shared with the visual and computation modules using data binding (see further). The simplest file to check out here is `polimero-csv.html`.

##### What's in a data module?
Any data module should:
* Define the `datapoints` and `file` properties.
* Have a `ready` function in which the `me.datapoints` object is populated.

The format of `datapoints` needs to be very well defined to make it usable in the other modules. Basically, it is *an array of JSON objects*. Each JSON object itself has at least the following keys:
* `id` - will typically be `polimero_id_` with a number
* `data` - a JSON object with any data, e.g. `{sepalLength: 5.2, petalLength: 4.1}`

The `id` value will be used to enable brushing/linking between different visuals (see later). So `datapoints` looks like this:
```
[
	{
		"id" : "polimero_id_0",
		"data" : {
			"species" : "Iris-setosa",
			"petalLength" : "1.4",
			"sepalWidth" : "3.5",
			"petalWidth" : "0.2",
			"sepalLength" : "5.1"
		}
	},
	{
		"id" : "polimero_id_1",
		"data" : {
			"petalWidth" : "0.2",
			"sepalLength" : "4.9",
			"species" : "Iris-setosa",
			"petalLength" : "1.4",
			"sepalWidth" : "3.0"
		}
	},
	{
		"id" : "polimero_id_2",
		"data" : {
			"sepalWidth" : "3.2",
			"petalLength" : "1.3",
			"species" : "Iris-setosa",
			"sepalLength" : "4.7",
			"petalWidth" : "0.2"
		}
	},
	{
		"id" : "polimero_id_3",
		"data" : {
			"petalWidth" : "0.2",
			"sepalLength" : "4.6",
			"species" : "Iris-setosa",
			"sepalWidth" : "3.1",
			"petalLength" : "1.5"
		}
	}
]
```

The `graph` data module also adds a `type` key, which is either `node` or `link`. It looks like this:
```
[
	{
		"id": "polimero_id_0",
		"type": "node",
		"data": {
			"name": "gene_1"
		}
	},
	{
		"id": "polimero_id_1",
		"type": "node",
		"data": {
			"name": "gene_2"
		}
	},
	{
		"id": "polimero_id_0_1",
		"type": "link",
		"source": 0,
		"target": 1,
		"source_id": "polimero_id_0",
		"target_id": "polimero_id_1"
		"data": {
			"weight": 2
		}
	}
]
```
This will make it possible to refer to the nodes or links separately in the visuals. Why store both nodes and links in the same datastructure `datapoints`? Because that makes the code portable: any data module just spits out a `datapoints` array. Could we have used a javascript object with keys `nodes` and `links`? Of course, but also that would break the portability.

#### Visual modules
Data loaded by one of the data modules becomes available in the `datapoints` variable within the visual modules. Check out `polimero-scatterplot.html` as an example.

What does a visual module file look like?
* It should load the `PolimeroBehavior` behaviour, as well as the `BrushableBehavior` if you want to use that visual for brushing and linking (see below).
* It should have a `_draw` function which add SVG elements using D3 based on `datapoints`.
* If you want to brush within that visual, you also have to include the brushing code here.

#### Computation modules
These are not implemented yet... The aim here is to group all functions that do transformations on the data (e.g. calculate histograms to show as barcharts). These calculations can in principle be done locally (in javascript) or reference a function on a spark cluster, for example.

### Inheritance and behaviours
Polymer 1.0 does not allow for inheritance of custom elements (yet?). To extract common functionality from different modules, we use *behaviours* instead.

Every visual module load the `PolimeroBehavior` module. In that module, we define a number of common properties with sensible default values. It also transforms the `dimensions` string used in many visuals (e.g. `dimensions="sepalLength, petalLength"`) into the `_dims` array (e.g. `["sepalLength","petalLength"]`). Also very important: it defines the `datapoints` property:
```
datapoints: {
	type: Array,
	value: [],
	notify: true,
	observer: "_draw"
},
```

*To check: do we still need to define this property in the data modules?*

As discussed above, the `datapoints` property is an array. `notify: true` means that any change of the contents that occurred in one of the data, visual or computational elements will trigger an event in the other elements which use this variable. And when that happens, the `_draw` function is executed in those other elements (`observer: "_draw"`). That's why each visual element needs to have a `_draw` function.

### Interaction between modules
#### Sharing data and functionality
For data to be shared between different modules, we need data binding in the `index.html` file using the double moustache. In the example at the top of the document, the value (`csv`) of the `datapoints` property is shared between `polimero-csv`, `polimero-scatterplot` and `polimero-radviz`. In addition, the `datapoints` property needs to be defined in each module. This is taken care of by defining it in `polimero-behavior.html` and loading that behaviour into the elements.

#### Brushing
To make brushing/linking possible, we use a shared `brushedmarks` variable. Brushing/linking happens in 4 steps:
1. The user selects a group of datapoints.
1. The IDs of these datapoints are added to the `brushedmarks` array.
1. Every time the `brushedmarks` array changes, the `_brushChanged` method in `brushable-behavior.html` is called for every module that loads the `BrushableBehavior` behaviour. This method gets all elements that are mentioned in the `brushedmarks` array, and sets their class to `brushed`.
1. The color of the marks that are brushed is changed using CSS selector.


## How to contribute
The data is available at bitbucket.org/vda-lab/polimero. We use the "feature branch" approach for collaboration (see https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow/). The `master` branch at the moment is only writable by Jan Aerts in order to keep some consistency in the initial stages of the library. To contribute, please clone the repository, create a new branch, make your changes, push your new branch to bitbucket, and send a pull request. So:
* `git clone git@bitbucket.org:vda-lab/polimero.git`
* `git checkout -b new_branch` (but please use a sensible name instead...)
* `bower install` (to get all the dependencies)
* Make your changes and commit to your local branch
* `git push -u origin new_branch`

You can compare the version of a file in your new branch with the one in master like this: `git diff new_branch master -- your_file`.

The maintainer can then get that new branch from bitbucket with the command `git checkout -b new_branch origin/new_branch`

### Relative paths issue
When developing, your polimero code will not be in the `bower_components` subdirectory (which it *will* be for the user). The `polyserve` command (to install with `npm install -g polyserve`) takes care of this.

* Create an index.html
* Run `polyserve`
* Goto http://localhost:8080/components/polimero/index.html

### Creating new modules

To create a new module, use elements/polimero-csv.html or elements/polimero-scatterplot.html as a starting point. These are more documented than the other elements.

## To do
* Move the element-specific brushing code to `brushable-behavior`.
* Make polimero available through bower.
* Document each element with example code at the top.
